const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema(
  {
    text: {
      type: String,
      required: true,
      trim: true
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    date: {
      type: Date,
      default: Date.now()
    },
    completed: {
      type: Boolean,
      default: false
    },

  },
  {
    collection: 'notes'
  }
);

noteSchema.methods.toJSON = function () {
  const note = this;
  const noteObject = note.toObject();

  delete noteObject.__v;
  noteObject.userId.toString();

  return noteObject;
};

module.exports.Note = mongoose.model('Note', noteSchema);
