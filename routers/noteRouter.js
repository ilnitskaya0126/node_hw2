const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { Note } = require('../schema/noteSchema.js');

router.get('/', authMiddleware, async (req, res) => {
  try {
    await req.user
      .populate({
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.offset)
        },
        path: 'notes',
      })
      .execPopulate();
    res.status(200).json({ notes: [...req.user.notes] });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }

});

router.get('/:id', authMiddleware, async (req, res) => {
  let _id = req.params.id;

  const note = await Note.findOne({ _id, userId: req.user._id });

  if (!note) {
    return res.status(400).json({ message: 'No such note' });
  }

  return res.status(200).send(note);

});

router.post('/', authMiddleware, async (req, res) => {
  
  const note = new Note({
    userId: req.user._id,
    text: req.body.text
  });

  try {
    await note.save();
    res.status(200).json({ message: 'Success' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }

});

router.put('/:id', authMiddleware, async (req, res) => {
  let _id = req.params.id;

  const note = await Note.findOne({ _id, userId: req.user._id });
  if (!note) {
    return res.status(400).json({ message: 'No such note' });
  }

  if(!req.body.text) {
    return res.status(400).json({ message: 'Enter text field' });
  }

  note.text = req.body.text;
  await note.save();
  return res.status(200).json({ message: 'Success' });

});

router.patch('/:id', authMiddleware, async (req, res) => {
  let _id = req.params.id;

  const note = await Note.findOne({ _id, userId: req.user._id });

  if (!note) {
    res.status(400).json({ message: 'No such note' });
  }

  note.completed = !note.completed;
  await note.save();
  res.status(200).json({ message: 'Success' });

});

router.delete('/:id', authMiddleware, async (req, res) => {
  let _id = req.params.id;

  const note = await Note.findOne({ _id, userId: req.user._id });

  if (!note) {
    res.status(400).json({ message: 'No such task' });
  }

  await Note.findOneAndDelete({_id,userId: req.user._id});
  return res.status(200).json({ message: 'Success' });

});

module.exports = router;
