const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const bcrypt = require('bcrypt');

router.get('/', authMiddleware, async (req, res) => {
  if (req.user) {
    res.status(200).json({ user: req.user });
  } else {
    res.status(400).json({ message: 'Please autheticate!' });
  }
});

router.delete('/', authMiddleware, async (req, res) => {
  try {
    await req.user.remove();
    res.status(200).json({ message: 'Success' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.patch('/', authMiddleware, async (req, res) => {
  const user = req.user;
  const { oldPassword, newPassword } = req.body;

  if(!oldPassword || !newPassword) {
    return  res.status(400).json({ message: 'Wrong data, please, try again' });
  }

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({ message: `Wrong password!` });
  }

  if (newPassword !== oldPassword) {
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    return res.status(200).json({ message: 'Success' });
  }
  

});

module.exports = router;
