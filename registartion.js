const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('./config.js');
const { User } = require('./schema/userSchema.js');

module.exports.registration = async (req, res) => {
  const { username, password } = req.body;

  if(!username || !password) {
    return res.status(400).json({ message: 'Enter your password and login' });
  }

  let user = await User.find({ username });
  console.log(user);

  if(user.length !== 0) {
    return res.status(400).json({ message: 'User is already exist!' });
  }

  user = new User({
    username: username,
    password: await bcrypt.hash(password, 10)
  });

  const token = jwt.sign({ username: user.username, _id: user._id },JWT_SECRET);

  await user.save();

  return res.status(200).json({ message: 'Success' });
};

module.exports.login = async (req, res) => {
  const { username, password } = req.body;

  if(!username || !password) {
    return res.status(400).json({ message: 'Enter correct password and login' });
  }

  const user = await User.findOne({ username });

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({ message: `Incorrect username or password, please try again` });
  }

  const token = jwt.sign({ username: user.username, _id: user._id },JWT_SECRET);
  
  res.status(200).json({ message: 'Success', jwt_token: token });
};
