const jwt = require('jsonwebtoken');
const { User } = require('../schema/userSchema.js');
const { JWT_SECRET } = require('../config.js');

const authMiddleware = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401).json({ message: `Http header 'Authorization' not found!` });
  }

  const [tokenType, token] = header.split(" ");

  if (!token) {
    return res.status(401).json({ message: `No JWT token found` });
  }

  const userData = jwt.verify(token,JWT_SECRET);

  if(!userData) {
    return res.status(401).json({ message: `Invalid token` });
  }

  const user = await User.findOne({
    _id: userData._id,
  });

  if (!user) {
    res.status(400).json({ message: 'Authenticate' });
  }

  req.token = token;
  req.user = user;
  next();
};

module.exports = authMiddleware;


