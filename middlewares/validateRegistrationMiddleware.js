const Joi = require('joi');

module.exports.validateRegistrationMiddleware = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().pattern(/^[a-zA-Z0-9]{6,30}$/).required()
  });

  const { error, value } = schema.validate(req.body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({message: 'Invalid data'});
  }
  next();
};
